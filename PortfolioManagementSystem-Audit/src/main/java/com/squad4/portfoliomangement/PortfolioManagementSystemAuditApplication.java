package com.squad4.portfoliomangement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfolioManagementSystemAuditApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortfolioManagementSystemAuditApplication.class, args);
	}

}
