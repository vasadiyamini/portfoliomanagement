package com.squad4.portfoliomangement.service;

import com.squad4.portfoliomangement.dto.GetAuditReturnDto;

public interface GetAuditService {

	GetAuditReturnDto getAudits(Long portfolioid, Integer pageNumber, Integer pageSize);

}
