package com.squad4.portfoliomangement.exception;

import com.squad4.portfoliomangement.util.CustomCode;

public class ResultNotFoundInCurrentPageException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResultNotFoundInCurrentPageException() {
		super(CustomCode.NO_RESULTIN_CURRENTPAGE_EXCEPTION_MESSAGE, CustomCode.NO_RESULTIN_CURRENTPAGE_EXCEPTION_CODE);
	}

}
