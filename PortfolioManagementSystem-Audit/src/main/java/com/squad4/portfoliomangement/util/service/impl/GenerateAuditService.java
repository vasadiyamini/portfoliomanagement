package com.squad4.portfoliomangement.util.service.impl;

import java.time.LocalDate;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squad4.portfoliomangement.entity.Audit;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.repository.AuditRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GenerateAuditService {
	
	private final AuditRepository auditRepository;
	
    @KafkaListener(topics = "audit",groupId = "group-id")
    public void consume(ConsumerRecord<String, Object> records) throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(records.value().toString());
		Long customerId= node.get("portfolioId").asLong();       
		Long instrumentId=node.get("instrumentId").asLong();
		Integer noOfUnits=node.get("noOfUnits").asInt();
		String transactionRef=node.get("transactionRef").asText();	
		TradeType tradeType=TradeType.valueOf(node.get("tradeType").asText());
		LocalDate auditDate=LocalDate.parse(node.get("auditDate").asText());
        Audit audit=new Audit(customerId, transactionRef, instrumentId, noOfUnits, tradeType, auditDate, instrumentId);
        auditRepository.save(audit);
    }
}
