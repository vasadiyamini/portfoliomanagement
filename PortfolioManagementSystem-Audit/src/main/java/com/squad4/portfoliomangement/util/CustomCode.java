package com.squad4.portfoliomangement.util;

public class CustomCode {
	/**
	 * Successful Code And Message
	 */

	public static final String AUDIT_DETAILS_FETCHED_SUCCESSFULLY_MESSAGE = "Auidt details fetched successfully";
	public static final String AUDIT_DETAILS_FETCHED_SUCCESSFULLY_CODE = "SUCCESS002";

	/**
	 * Exception Code
	 */
	public static final String CUSTOMER_NOTFOUND_EXCEPTION_MESSAGE = "Customer NotFound";
	public static final String CUSTOMER_NOTFOUND_EXCEPTION_CODE = "EX001";
	public static final String NO_RESULTIN_CURRENTPAGE_EXCEPTION_MESSAGE = " No Resultin CurrentPage";
	public static final String NO_RESULTIN_CURRENTPAGE_EXCEPTION_CODE = "EX002";

	private CustomCode() {
		super();
	}
}
