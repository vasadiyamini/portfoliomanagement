package com.squad4.portfoliomangement.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long auidId;
	private String transactionRef;
	private Long instrumentId;
	private Integer noOfUnits;
	@Enumerated(EnumType.STRING)
	private TradeType tradeType;
	private LocalDate auditDate;
	private Long portfolioId;
}
