package com.squad4.portfoliomangement.serviceimpl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.GetAuditDto;
import com.squad4.portfoliomangement.dto.GetAuditReturnDto;
import com.squad4.portfoliomangement.entity.Audit;
import com.squad4.portfoliomangement.exception.CustomerNotFoundException;
import com.squad4.portfoliomangement.exception.ResultNotFoundInCurrentPageException;
import com.squad4.portfoliomangement.repository.AuditRepository;
import com.squad4.portfoliomangement.service.GetAuditService;
import com.squad4.portfoliomangement.util.CustomCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class GetAuditServiceImpl implements GetAuditService {
	private final AuditRepository auditRepository;

	/**
	 * 
	 * @param portfolioid it well fetch the customer audits
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public GetAuditReturnDto getAudits(Long portfolioid, Integer pageNumber, Integer pageSize) {
		List<Audit> audit = auditRepository.findByPortfolioId(portfolioid);
		log.warn("Customer Not Exist");
		if (audit.isEmpty()) {
			throw new CustomerNotFoundException();
		}
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<Audit> audits = auditRepository.findByPortfolioId(portfolioid, pageable);
		log.warn("ResultNotFoundInCurrentPageException");
		if (!audits.hasContent()) {
			throw new ResultNotFoundInCurrentPageException();
		}
		List<GetAuditDto> getAuditDto = audits.getContent().stream().map(request -> {
			GetAuditDto getAudit = new GetAuditDto();
			BeanUtils.copyProperties(request, getAudit);
			getAudit.setAuditDate(request.getAuditDate());
			getAudit.setInstrumentId(request.getInstrumentId());
			getAudit.setTransactionRef(request.getTransactionRef());
			getAudit.setTradeType(request.getTradeType());
			return getAudit;
		}).toList();
		log.info("values fetched successfully");
		return GetAuditReturnDto.builder()
				.apiResponse(ApiResponse.builder().code(CustomCode.AUDIT_DETAILS_FETCHED_SUCCESSFULLY_CODE)
						.message(CustomCode.AUDIT_DETAILS_FETCHED_SUCCESSFULLY_MESSAGE).build())
				.response(new PageImpl<>(getAuditDto)).build();
	}

}
