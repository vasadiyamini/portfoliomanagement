package com.squad4.portfoliomangement.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.portfoliomangement.entity.Audit;

public interface AuditRepository extends JpaRepository<Audit, Long> {

	List<Audit> findByPortfolioId(Long portfolioId);

	Page<Audit> findByPortfolioId(Long portfoliod, Pageable pageable);

}
