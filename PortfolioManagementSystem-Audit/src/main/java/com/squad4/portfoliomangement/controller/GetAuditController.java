package com.squad4.portfoliomangement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.portfoliomangement.dto.GetAuditReturnDto;
import com.squad4.portfoliomangement.service.GetAuditService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class GetAuditController {
	private final GetAuditService getAuditService;

	/**
	 * 
	 * @param portfolioid it well fetch the customer audits
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/{portfolioid}")
	ResponseEntity<GetAuditReturnDto> getAudit(@PathVariable Long portfolioid,
			@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "1") Integer pageSize) {
		log.info("Get users audit values");
		return ResponseEntity.status(HttpStatus.OK).body(getAuditService.getAudits(portfolioid, pageNumber, pageSize));
	}

}
