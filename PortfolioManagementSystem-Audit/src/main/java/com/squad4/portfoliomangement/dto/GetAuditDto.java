package com.squad4.portfoliomangement.dto;

import java.time.LocalDate;

import com.squad4.portfoliomangement.entity.TradeType;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetAuditDto {

	private String transactionRef;
	private Long instrumentId;
	@Enumerated(EnumType.STRING)
	private TradeType tradeType;
	private LocalDate auditDate;
}
