package com.squad4.portfoliomangement.dto;

import org.springframework.data.domain.Page;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetAuditReturnDto {
	private ApiResponse apiResponse;
	private Page<GetAuditDto> response;
}
