package com.squad4.portfoliomangement.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.squad4.portfoliomangement.dto.GetAuditReturnDto;
import com.squad4.portfoliomangement.entity.Audit;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.exception.CustomerNotFoundException;
import com.squad4.portfoliomangement.exception.ResultNotFoundInCurrentPageException;
import com.squad4.portfoliomangement.repository.AuditRepository;
import com.squad4.portfoliomangement.util.CustomCode;

@ExtendWith(MockitoExtension.class)

class GetAuditServiceImplTest {
	@Mock
	private AuditRepository auditRepository;
	@InjectMocks
	private GetAuditServiceImpl getAuditServiceImpl;
	private List<Audit> audit;

	@Test
	void testGetAudits_Success() {
		Long portfolioId = 1L;
		Integer pageNumber = 0;
		Integer pageSize = 10;
		List<Audit> auditList = new ArrayList<>();
		auditList.add(new Audit(1L, "Ref123", 100L, 10, TradeType.BUY, LocalDate.now(), portfolioId));
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<Audit> page = new PageImpl<>(auditList);
		when(auditRepository.findByPortfolioId(portfolioId)).thenReturn(auditList);
		when(auditRepository.findByPortfolioId(portfolioId, pageable)).thenReturn(page);
		GetAuditReturnDto result = getAuditServiceImpl.getAudits(portfolioId, pageNumber, pageSize);
		assertNotNull(result);
		assertEquals(CustomCode.AUDIT_DETAILS_FETCHED_SUCCESSFULLY_CODE, result.getApiResponse().getCode());
		assertEquals(CustomCode.AUDIT_DETAILS_FETCHED_SUCCESSFULLY_MESSAGE, result.getApiResponse().getMessage());
		assertEquals(auditList.size(), result.getResponse().getContent().size());
	}

	@Test
	void testGetAudits_CustomerNotFoundException() {
		Long portfolioId = 1L;
		Integer pageNumber = 0;
		Integer pageSize = 10;
		when(auditRepository.findByPortfolioId(portfolioId)).thenReturn(new ArrayList<>());
		assertThrows(CustomerNotFoundException.class,
				() -> getAuditServiceImpl.getAudits(portfolioId, pageNumber, pageSize));
	}

	@Test
	void testGetAudits_ResultNotFoundInCurrentPageException() {
		Long portfolioId = 1L;
		Integer pageNumber = 0;
		Integer pageSize = 10;
		List<Audit> auditList = new ArrayList<>();
		auditList.add(new Audit(1L, "Ref123", 100L, 10, TradeType.BUY, LocalDate.now(), portfolioId));
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<Audit> emptyPage = new PageImpl<>(new ArrayList<>());
		when(auditRepository.findByPortfolioId(portfolioId)).thenReturn(auditList);
		when(auditRepository.findByPortfolioId(portfolioId, pageable)).thenReturn(emptyPage);
		assertThrows(ResultNotFoundInCurrentPageException.class,
				() -> getAuditServiceImpl.getAudits(portfolioId, pageNumber, pageSize));
	}
}
