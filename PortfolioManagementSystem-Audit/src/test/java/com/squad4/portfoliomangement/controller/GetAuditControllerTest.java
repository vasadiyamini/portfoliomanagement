package com.squad4.portfoliomangement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.portfoliomangement.dto.GetAuditReturnDto;
import com.squad4.portfoliomangement.service.GetAuditService;

@ExtendWith(SpringExtension.class)
class GetAuditControllerTest {
	@Mock
	private GetAuditService getAuditService;
	@InjectMocks
	private GetAuditController getAuditController;

	@Test
	void testGetAudit() {
		Long portfolioid = 1l;
		Integer pageNumber = 0;
		Integer pageSize = 1;
		GetAuditReturnDto getAuditReturnDto = GetAuditReturnDto.builder().apiResponse(null).response(null).build();
		when(getAuditService.getAudits(portfolioid, pageNumber, pageSize)).thenReturn(getAuditReturnDto);
		ResponseEntity<GetAuditReturnDto> responseEntity = getAuditController.getAudit(portfolioid, pageNumber,
				pageSize);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

}
