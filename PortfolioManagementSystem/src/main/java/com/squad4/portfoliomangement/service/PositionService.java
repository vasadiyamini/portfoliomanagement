package com.squad4.portfoliomangement.service;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.TradeEntryDto;
import com.squad4.portfoliomangement.entity.TradeType;

public interface PositionService {
	ApiResponse tradeEntry(TradeType tradeType,TradeEntryDto tradeEntryDto);
}
