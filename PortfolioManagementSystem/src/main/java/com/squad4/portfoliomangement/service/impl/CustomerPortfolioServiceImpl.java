package com.squad4.portfoliomangement.service.impl;

import org.springframework.stereotype.Service;

import com.squad4.portfoliomangement.dto.MyResultDto;
import com.squad4.portfoliomangement.dto.PortfolioResponseDto;
import com.squad4.portfoliomangement.entity.CustomerPortfolio;
import com.squad4.portfoliomangement.exception.CustomerNotFoundException;
import com.squad4.portfoliomangement.repository.CustomerPortfolioRepository;
import com.squad4.portfoliomangement.service.CustomerPortfolioService;
import com.squad4.portfoliomangement.util.CustomCode;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerPortfolioServiceImpl implements CustomerPortfolioService{
 
	private final CustomerPortfolioRepository customerPortfolioRepository;
 
	@Override
	public PortfolioResponseDto getcustomerportfolio(Long customerId) {
		CustomerPortfolio portfolio = customerPortfolioRepository.findById(customerId)
				.orElseThrow(()-> new CustomerNotFoundException());
		MyResultDto resultDto = MyResultDto.builder()
				.customerName(portfolio.getCustomerName())
				.currentPerformance(portfolio.getCurrentPerformance())
				.investmentStrategy(portfolio.getInvestmentStrategy())
				.portfolioNumber(portfolio.getPortfolioNumber())
				.portfolioValue(portfolio.getPortfolioValue())
				.build();
		
	 		   return PortfolioResponseDto.builder()
	 				   .resultDto(resultDto)
	 				   .message(CustomCode.PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_MESSAGE)
	 				   .code(CustomCode.PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_CODE)
	 				   .build();
		
	}
	
}