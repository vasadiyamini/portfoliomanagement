package com.squad4.portfoliomangement.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.ResultDto;
import com.squad4.portfoliomangement.dto.TradeEntryDto;
import com.squad4.portfoliomangement.entity.CustomerPortfolio;
import com.squad4.portfoliomangement.entity.Instrument;
import com.squad4.portfoliomangement.entity.Position;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.exception.CustomerNotFoundException;
import com.squad4.portfoliomangement.exception.InstrumentNotFoundException;
import com.squad4.portfoliomangement.exception.InstrumentNotPurchasedException;
import com.squad4.portfoliomangement.exception.InsufficientQuantityAvailableToBuyException;
import com.squad4.portfoliomangement.exception.InsufficientQuantityAvailableToSellException;
import com.squad4.portfoliomangement.exception.RequestedUnitsnotAvailableException;
import com.squad4.portfoliomangement.repository.CustomerPortfolioRepository;
import com.squad4.portfoliomangement.repository.InstrumentRepository;
import com.squad4.portfoliomangement.repository.PositionRepository;
import com.squad4.portfoliomangement.service.PositionService;
import com.squad4.portfoliomangement.util.CustomCode;
import com.squad4.portfoliomangement.util.KafkaSender;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class PositionServiceImpl implements PositionService{
	
	private final CustomerPortfolioRepository customerPortfolioRepository;
	private final InstrumentRepository  instrumentRepository;
	private final PositionRepository positionRepository;
	private final KafkaSender kafkaSender;
	/**
	 * Trade Entry Details functionality
	 */
	@Override
	public ApiResponse tradeEntry(TradeType tradeType, TradeEntryDto tradeEntryDto) {
		
		CustomerPortfolio customerPortfolio=customerPortfolioRepository.findById(tradeEntryDto.portfolioId()).orElseThrow(CustomerNotFoundException::new);
		Instrument instrument=instrumentRepository.findById(tradeEntryDto.instrumentId()).orElseThrow(InstrumentNotFoundException::new);
		if(instrument.getUnits()< tradeEntryDto.units()) {
			log.warn(CustomCode.REQUESTED_UNITS_NOTAVAILABLE_EXCEPTION_CODE);
			throw new RequestedUnitsnotAvailableException();
		}
		Optional<Position> position=positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument);
		if(position.isEmpty() && tradeType.equals(TradeType.SELL)) {
			log.warn(CustomCode.INSTRUMENTSID_NOTFOUND_EXCEPTION_CODE);
            throw new InstrumentNotPurchasedException();
		}
		
        if(position.isPresent() && tradeType.equals(TradeType.SELL) &&  position.get().getNumberOfUnits()< tradeEntryDto.units()) { 
           log.warn(CustomCode.INSUFFICIENT_QUANTITY_AVAILABLE_TOSELL_EXCEPTION_MESSAGE);
        	throw new InsufficientQuantityAvailableToSellException();
        }
       
        if(instrument.getUnits()<tradeEntryDto.units()) {
        	log.warn(CustomCode.INSUFFICIENT_QUANTITY_AVAILABLE_TOBUY_EXCEPTION_MESSAGE);
        	throw new InsufficientQuantityAvailableToBuyException();
        }
        
        double totalPrice= instrument.getInstrumentValue() * tradeEntryDto.units();
        int value=0;
        if(tradeType.equals(TradeType.BUY)) { 
        	instrument.setUnits(instrument.getUnits()-tradeEntryDto.units());
        	customerPortfolio.setPortfolioValue(customerPortfolio.getPortfolioValue().add(BigDecimal.valueOf(totalPrice)));
        	
        }
        
        if(tradeType.equals(TradeType.SELL)) {
        	position.get().setNumberOfUnits(position.get().getNumberOfUnits()-tradeEntryDto.units());
        	customerPortfolio.setPortfolioValue(customerPortfolio.getPortfolioValue().subtract(BigDecimal.valueOf(totalPrice)));
        	
        }
       if(position.isPresent()) {
    	  value=switch(tradeType) {
    	   case BUY ->
    		   position.get().getNumberOfUnits()+tradeEntryDto.units();
    		  
    	   case SELL ->
    		   position.get().getNumberOfUnits()-tradeEntryDto.units();
    		   
    	    
    	   };
       }
        
        Position position2;
        if(position.isPresent()) {
        	position2=position.get();
        	position2.setCustomerPortfolio(customerPortfolio);
        	position2.setDate(LocalDate.now());
        	position2.setInstrument(instrument);
        	position2.setNumberOfUnits(value);
        }
        else
        	position2=Position.builder().customerPortfolio(customerPortfolio).date(LocalDate.now()).instrument(instrument).numberOfUnits(tradeEntryDto.units()).build();
        
        instrumentRepository.save(instrument);
        positionRepository.save(position2);
        customerPortfolioRepository.save(customerPortfolio);
        
       
        log.warn("kafka sent");
        kafkaSender.send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        return ApiResponse.builder().message(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE).code(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_CODE).build();
	}
}
