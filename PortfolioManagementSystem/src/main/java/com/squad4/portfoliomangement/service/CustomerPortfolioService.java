package com.squad4.portfoliomangement.service;

import com.squad4.portfoliomangement.dto.PortfolioResponseDto;

public interface CustomerPortfolioService {
	PortfolioResponseDto getcustomerportfolio(Long customerid);
}
