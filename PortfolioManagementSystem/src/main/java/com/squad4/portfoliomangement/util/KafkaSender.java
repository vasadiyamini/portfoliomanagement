package com.squad4.portfoliomangement.util;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.squad4.portfoliomangement.dto.ResultDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaSender {
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(ResultDto resultDto) {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, ResultDto> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());	
		
		log.info("Message Sent: "+resultDto);
		
		ProducerRecord<String, ResultDto> record1 = new ProducerRecord<String, ResultDto>("audit", resultDto);
		log.info(""+record1);
	kafkaProducer.send(record1);
		
 
		
	}
 
 
}