package com.squad4.portfoliomangement.util;

public class CustomCode {
	/**
	 * Successful Code And Message
	 */

	public static final String PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_MESSAGE = "portfolio summary fetched successfully";
	public static final String PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_CODE = "SUCCESS001";
	public static final String AUDIT_DETAILS_FETCHED_SUCCESSFULLY_MESSAGE = "Auidt details fetched successfully";
	public static final String AUDIT_DETAILS_FETCHED_SUCCESSFULLY_CODE = "SUCCESS002";
	public static final String CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE = "Customer successfuly traded";
	public static final String CUSTOMER_SUCCESSFULLY_TRADED_CODE = "SUCCESS003";

	/**
	 * Exception Code
	 */
	public static final String CUSTOMER_NOTFOUND_EXCEPTION_MESSAGE = "Customer NotFound";
	public static final String CUSTOMER_NOTFOUND_EXCEPTION_CODE = "EX001";
	public static final String NO_RESULTIN_CURRENTPAGE_EXCEPTION_MESSAGE = " No Resultin CurrentPage";
	public static final String NO_RESULTIN_CURRENTPAGE_EXCEPTION_CODE = "EX002";
	public static final String INSTRUMENTSID_NOTFOUND_EXCEPTION_MESSAGE = "InstrumentsId NotFound";
	public static final String INSTRUMENTSID_NOTFOUND_EXCEPTION_CODE = "EX003";
	public static final String REQUESTED_UNITS_NOTAVAILABLE_EXCEPTION_MESSAGE = "Requested Units NotAvailable";
	public static final String REQUESTED_UNITS_NOTAVAILABLE_EXCEPTION_CODE = "EX004";
	public static final String INSUFFICIENT_QUANTITY_AVAILABLE_TOSELL_EXCEPTION_MESSAGE = "Insufficient Quantity Available To Sell";
	public static final String INSUFFICIENT_QUANTITY_AVAILABLE_TOSELL_EXCEPTION_CODE = "EX005";
	public static final String INSUFFICIENT_QUANTITY_AVAILABLE_TOBUY_EXCEPTION_MESSAGE = "Insufficient Quantity Available To Sell";
	public static final String INSUFFICIENT_QUANTITY_AVAILABLE_TOBUY_EXCEPTION_CODE = "EX007";

	private CustomCode() {
		super();
	}
}
