package com.squad4.portfoliomangement.entity;

import java.time.LocalDate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Position {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long positionId;
	@ManyToOne(cascade = CascadeType.ALL)
	private CustomerPortfolio customerPortfolio;
	@ManyToOne(cascade = CascadeType.ALL)
	private Instrument instrument;
	private Integer numberOfUnits;
	private LocalDate date;
}
