package com.squad4.portfoliomangement.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerPortfolio {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long portfolioId;
	private Long customerId;
	private String customerName;
	private String portfolioNumber;
	private BigDecimal portfolioValue;
	private Double currentPerformance;
	@Enumerated(EnumType.STRING)
	private InvestmentStrategy investmentStrategy;

}
