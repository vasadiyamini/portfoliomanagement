package com.squad4.portfoliomangement.dto;

import lombok.Builder;

@Builder
public record TradeEntryDto(Long portfolioId,Long instrumentId,Integer units) {

}
