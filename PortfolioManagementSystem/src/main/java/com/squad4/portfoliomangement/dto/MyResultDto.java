package com.squad4.portfoliomangement.dto;

import java.math.BigDecimal;

import com.squad4.portfoliomangement.entity.InvestmentStrategy;

import lombok.Builder;

@Builder
public record MyResultDto (String customerName,
		String portfolioNumber,
		BigDecimal portfolioValue,
		Double currentPerformance,
		InvestmentStrategy investmentStrategy){
}
	 