package com.squad4.portfoliomangement.dto;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultDto {
	private String transactionRef;
	private Long instrumentId;
	private Integer noOfUnits;
	@Enumerated(EnumType.STRING)
	private String tradeType;
	private String auditDate;
	private Long portfolioId;
}
