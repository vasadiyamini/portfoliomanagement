package com.squad4.portfoliomangement.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, String code) {

}
