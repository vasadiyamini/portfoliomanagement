package com.squad4.portfoliomangement.dto;

import lombok.Builder;

@Builder
public record PortfolioResponseDto(String message, String code, MyResultDto resultDto) {
 
}
 
