package com.squad4.portfoliomangement.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.portfoliomangement.entity.Instrument;

public interface InstrumentRepository extends JpaRepository<Instrument, Long> {

}
