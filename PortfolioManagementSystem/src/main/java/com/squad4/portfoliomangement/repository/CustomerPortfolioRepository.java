package com.squad4.portfoliomangement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.portfoliomangement.entity.CustomerPortfolio;

public interface CustomerPortfolioRepository extends JpaRepository<CustomerPortfolio, Long> {
	
}
