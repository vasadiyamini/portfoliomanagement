package com.squad4.portfoliomangement.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.portfoliomangement.entity.CustomerPortfolio;
import com.squad4.portfoliomangement.entity.Instrument;
import com.squad4.portfoliomangement.entity.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {
	Optional<Position> findByCustomerPortfolioAndInstrument(CustomerPortfolio customerPortfolio,Instrument instrument);
}
