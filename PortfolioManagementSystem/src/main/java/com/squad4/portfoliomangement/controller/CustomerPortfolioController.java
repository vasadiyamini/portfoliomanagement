package com.squad4.portfoliomangement.controller;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.portfoliomangement.dto.PortfolioResponseDto;
import com.squad4.portfoliomangement.service.CustomerPortfolioService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class CustomerPortfolioController {
 
	private final CustomerPortfolioService customerPortfolioService;
 
	@GetMapping("/portfolio/{customerId}")
	public ResponseEntity<PortfolioResponseDto> getcustomerportolio(@PathVariable Long customerId) {
		log.info("PortFolioSummary Fetched Successsfully");
		return ResponseEntity.status(HttpStatus.OK).body(customerPortfolioService.getcustomerportfolio(customerId));
	}
}