package com.squad4.portfoliomangement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.TradeEntryDto;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.service.PositionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class PositionController {
	
	private final PositionService positionService;

	@PostMapping("/protfolios/instruments")
	public ResponseEntity<ApiResponse> tradeEntry(@RequestParam TradeType tradeType,@RequestBody TradeEntryDto tradeEntryDto){
		return new ResponseEntity<>(positionService.tradeEntry(tradeType, tradeEntryDto),HttpStatus.CREATED);
	}
}
