package com.squad4.portfoliomangement.exception;

import com.squad4.portfoliomangement.util.CustomCode;

public class CustomerNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException() {
		super(CustomCode.CUSTOMER_NOTFOUND_EXCEPTION_MESSAGE, CustomCode.CUSTOMER_NOTFOUND_EXCEPTION_CODE);

	}

}
