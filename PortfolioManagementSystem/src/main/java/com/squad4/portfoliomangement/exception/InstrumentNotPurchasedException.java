package com.squad4.portfoliomangement.exception;

public class InstrumentNotPurchasedException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InstrumentNotPurchasedException() {
		super("Instrument not purchased", "EX006");

	}

}
