package com.squad4.portfoliomangement.exception;

import com.squad4.portfoliomangement.util.CustomCode;

public class InsufficientQuantityAvailableToSellException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientQuantityAvailableToSellException() {
		super(CustomCode.INSUFFICIENT_QUANTITY_AVAILABLE_TOSELL_EXCEPTION_MESSAGE, CustomCode.INSUFFICIENT_QUANTITY_AVAILABLE_TOSELL_EXCEPTION_CODE);

	}

}
