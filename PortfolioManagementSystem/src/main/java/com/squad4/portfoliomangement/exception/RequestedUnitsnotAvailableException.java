package com.squad4.portfoliomangement.exception;

import com.squad4.portfoliomangement.util.CustomCode;

public class RequestedUnitsnotAvailableException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RequestedUnitsnotAvailableException() {
		super(CustomCode.REQUESTED_UNITS_NOTAVAILABLE_EXCEPTION_MESSAGE, CustomCode.REQUESTED_UNITS_NOTAVAILABLE_EXCEPTION_CODE);

	}

}
