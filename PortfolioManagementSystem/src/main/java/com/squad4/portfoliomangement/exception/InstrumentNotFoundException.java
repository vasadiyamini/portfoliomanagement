package com.squad4.portfoliomangement.exception;

import com.squad4.portfoliomangement.util.CustomCode;

public class InstrumentNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InstrumentNotFoundException() {
		super(CustomCode.INSTRUMENTSID_NOTFOUND_EXCEPTION_MESSAGE, CustomCode.INSTRUMENTSID_NOTFOUND_EXCEPTION_MESSAGE);

	}

}
