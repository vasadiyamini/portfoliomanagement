package com.squad4.portfoliomangement.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.squad4.portfoliomangement.dto.MyResultDto;
import com.squad4.portfoliomangement.dto.PortfolioResponseDto;
import com.squad4.portfoliomangement.entity.CustomerPortfolio;
import com.squad4.portfoliomangement.entity.InvestmentStrategy;
import com.squad4.portfoliomangement.exception.CustomerNotFoundException;
import com.squad4.portfoliomangement.repository.CustomerPortfolioRepository;
import com.squad4.portfoliomangement.util.CustomCode;

@ExtendWith(MockitoExtension.class)
class CustomerPortfolioServiceImplTest {
 
    @Mock
    CustomerPortfolioRepository customerPortfolioRepository;
 
    @InjectMocks
    CustomerPortfolioServiceImpl customerPortfolioService;
    
    @Test
    void getPortfolioSuccessfully() {
    	Long customerId = 1L;
    	CustomerPortfolio customerPortfolio = CustomerPortfolio.builder()
    			.customerId(customerId)
    			.build();
    	MyResultDto result = MyResultDto.builder().
    			customerName("kavin").portfolioNumber("12345").
    			currentPerformance(76D).investmentStrategy(InvestmentStrategy.SAFE).
    			portfolioValue(BigDecimal.valueOf(3423D)).build();
    	
    	when(customerPortfolioRepository.findById(customerId)).thenReturn(Optional.of(customerPortfolio));
    	PortfolioResponseDto getcustomerportfolio = customerPortfolioService.getcustomerportfolio(customerId);
    	assertEquals(CustomCode.PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_CODE,"SUCCESS001");
    }
 
    
 
 
 
    @Test
    void testGetCustomerPortfolio_CustomerNotFound() {
       
        Long customerId = 1L;
 
       Mockito.when(customerPortfolioRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
 
        
        assertThrows(CustomerNotFoundException.class, () -> customerPortfolioService.getcustomerportfolio(customerId));
    }
    
   
}