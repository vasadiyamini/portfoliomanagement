package com.squad4.portfoliomangement.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.ResultDto;
import com.squad4.portfoliomangement.dto.TradeEntryDto;
import com.squad4.portfoliomangement.entity.CustomerPortfolio;
import com.squad4.portfoliomangement.entity.Instrument;
import com.squad4.portfoliomangement.entity.Position;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.exception.InstrumentNotPurchasedException;
import com.squad4.portfoliomangement.exception.RequestedUnitsnotAvailableException;
import com.squad4.portfoliomangement.repository.CustomerPortfolioRepository;
import com.squad4.portfoliomangement.repository.InstrumentRepository;
import com.squad4.portfoliomangement.repository.PositionRepository;
import com.squad4.portfoliomangement.util.CustomCode;
import com.squad4.portfoliomangement.util.KafkaSender;

@ExtendWith(SpringExtension.class)
class PositionServiceImplTest {
	@Mock
	CustomerPortfolioRepository customerPortfolioRepository;
	@Mock
	InstrumentRepository  instrumentRepository;
	@Mock
	PositionRepository positionRepository;
	@Mock
	KafkaSender kafkaSender;
	@InjectMocks
	PositionServiceImpl positionServiceImpl;
	@Test
	void testSuccess() {
		TradeType tradeType=TradeType.BUY;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		CustomerPortfolio customerPortfolio=CustomerPortfolio.builder().portfolioId(1L).portfolioValue(BigDecimal.valueOf(100)).build();
		Instrument instrument=Instrument.builder().instrumentId(1L).units(100).instrumentValue(120D).build();
		Position position=Position.builder().customerPortfolio(customerPortfolio).instrument(instrument).numberOfUnits(10).build();
		Mockito.when(customerPortfolioRepository.findById(tradeEntryDto.portfolioId())).thenReturn(Optional.of(customerPortfolio));
		Mockito.when(instrumentRepository.findById(tradeEntryDto.instrumentId())).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument)).thenReturn(Optional.of(position));
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
        Mockito.when(positionRepository.save(position)).thenReturn(position);
        Mockito.when(customerPortfolioRepository.save(customerPortfolio)).thenReturn(customerPortfolio);
        doNothing().when(kafkaSender).send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        ApiResponse apiResponse=positionServiceImpl.tradeEntry(tradeType, tradeEntryDto);
        assertEquals(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE, apiResponse.message());
	}
	
	
	@Test
	void testSuccess1() {
		TradeType tradeType=TradeType.SELL;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		CustomerPortfolio customerPortfolio=CustomerPortfolio.builder().portfolioId(1L).portfolioValue(BigDecimal.valueOf(100)).build();
		Instrument instrument=Instrument.builder().instrumentId(1L).units(100).instrumentValue(120D).build();
		Position position=Position.builder().customerPortfolio(customerPortfolio).instrument(instrument).numberOfUnits(10).build();
		Mockito.when(customerPortfolioRepository.findById(tradeEntryDto.portfolioId())).thenReturn(Optional.of(customerPortfolio));
		Mockito.when(instrumentRepository.findById(tradeEntryDto.instrumentId())).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument)).thenReturn(Optional.of(position));
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
        Mockito.when(positionRepository.save(position)).thenReturn(position);
        Mockito.when(customerPortfolioRepository.save(customerPortfolio)).thenReturn(customerPortfolio);
        doNothing().when(kafkaSender).send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        ApiResponse apiResponse=positionServiceImpl.tradeEntry(tradeType, tradeEntryDto);
        assertEquals(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE, apiResponse.message());
	}
	
	@Test
	void testSuccess2() {
		TradeType tradeType=TradeType.BUY;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		CustomerPortfolio customerPortfolio=CustomerPortfolio.builder().portfolioId(1L).portfolioValue(BigDecimal.valueOf(100)).build();
		Instrument instrument=Instrument.builder().instrumentId(1L).units(100).instrumentValue(120D).build();
		Position position=Position.builder().customerPortfolio(customerPortfolio).instrument(instrument).numberOfUnits(10).build();
		Mockito.when(customerPortfolioRepository.findById(tradeEntryDto.portfolioId())).thenReturn(Optional.of(customerPortfolio));
		Mockito.when(instrumentRepository.findById(tradeEntryDto.instrumentId())).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument)).thenReturn(Optional.empty());
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
        Mockito.when(positionRepository.save(position)).thenReturn(position);
        Mockito.when(customerPortfolioRepository.save(customerPortfolio)).thenReturn(customerPortfolio);
        doNothing().when(kafkaSender).send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        ApiResponse apiResponse=positionServiceImpl.tradeEntry(tradeType, tradeEntryDto);
        assertEquals(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE, apiResponse.message());
	}
	
	@Test
	void testFailure() {
		TradeType tradeType=TradeType.BUY;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		CustomerPortfolio customerPortfolio=CustomerPortfolio.builder().portfolioId(1L).portfolioValue(BigDecimal.valueOf(100)).build();
		Instrument instrument=Instrument.builder().instrumentId(1L).units(1).instrumentValue(120D).build();
		Position position=Position.builder().customerPortfolio(customerPortfolio).instrument(instrument).numberOfUnits(10).build();
		Mockito.when(customerPortfolioRepository.findById(tradeEntryDto.portfolioId())).thenReturn(Optional.of(customerPortfolio));
		Mockito.when(instrumentRepository.findById(tradeEntryDto.instrumentId())).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument)).thenReturn(Optional.empty());
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
        Mockito.when(positionRepository.save(position)).thenReturn(position);
        Mockito.when(customerPortfolioRepository.save(customerPortfolio)).thenReturn(customerPortfolio);
        doNothing().when(kafkaSender).send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        assertThrows(RequestedUnitsnotAvailableException.class,()->positionServiceImpl.tradeEntry(tradeType, tradeEntryDto));
	}
	
	
	@Test
	void testFailure1() {
		TradeType tradeType=TradeType.SELL;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		CustomerPortfolio customerPortfolio=CustomerPortfolio.builder().portfolioId(1L).portfolioValue(BigDecimal.valueOf(100)).build();
		Instrument instrument=Instrument.builder().instrumentId(1L).units(100).instrumentValue(120D).build();
		Position position=Position.builder().customerPortfolio(customerPortfolio).instrument(instrument).numberOfUnits(10).build();
		Mockito.when(customerPortfolioRepository.findById(tradeEntryDto.portfolioId())).thenReturn(Optional.of(customerPortfolio));
		Mockito.when(instrumentRepository.findById(tradeEntryDto.instrumentId())).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByCustomerPortfolioAndInstrument(customerPortfolio, instrument)).thenReturn(Optional.empty());
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
        Mockito.when(positionRepository.save(position)).thenReturn(position);
        Mockito.when(customerPortfolioRepository.save(customerPortfolio)).thenReturn(customerPortfolio);
        doNothing().when(kafkaSender).send(ResultDto.builder().auditDate(LocalDate.now().toString()).instrumentId(instrument.getInstrumentId()).noOfUnits(tradeEntryDto.units()).portfolioId(customerPortfolio.getPortfolioId()).tradeType(tradeType.toString()).transactionRef("SQUAD4"+UUID.randomUUID().toString()).build());
        assertThrows(InstrumentNotPurchasedException.class,()->positionServiceImpl.tradeEntry(tradeType, tradeEntryDto));
	}
	
	
}
