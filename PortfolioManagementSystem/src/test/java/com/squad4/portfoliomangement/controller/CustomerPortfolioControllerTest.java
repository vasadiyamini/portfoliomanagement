package com.squad4.portfoliomangement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.portfoliomangement.dto.MyResultDto;
import com.squad4.portfoliomangement.dto.PortfolioResponseDto;
import com.squad4.portfoliomangement.entity.InvestmentStrategy;
import com.squad4.portfoliomangement.service.CustomerPortfolioService;
import com.squad4.portfoliomangement.util.CustomCode;

@ExtendWith(SpringExtension.class)
class CustomerPortfolioControllerTest {
 
	@Mock
	private CustomerPortfolioService customerPortfolioService;
 
	@InjectMocks
	private CustomerPortfolioController customerPortfolioController;
 
	@Test
	void testGetCustomerPortfolio_Success() {
		Long customerId = 1L;
		MyResultDto resultDto = MyResultDto.builder().customerName("kavin").portfolioNumber("1")
				.portfolioValue(BigDecimal.valueOf(12234.0)).currentPerformance(20D)
				.investmentStrategy(InvestmentStrategy.RISKY).build();
 
		PortfolioResponseDto dto = PortfolioResponseDto.builder().resultDto(resultDto)
				.message(CustomCode.PORTFOLIO_SUMMART_FETCHED_SUCCESSFULLY_MESSAGE).build();
		Mockito.when(customerPortfolioService.getcustomerportfolio(customerId)).thenReturn(dto);
		ResponseEntity<PortfolioResponseDto> getcustomerportolio = customerPortfolioController
				.getcustomerportolio(customerId);
		assertEquals(HttpStatus.OK, getcustomerportolio.getStatusCode());
 
	}
}