package com.squad4.portfoliomangement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.portfoliomangement.dto.ApiResponse;
import com.squad4.portfoliomangement.dto.TradeEntryDto;
import com.squad4.portfoliomangement.entity.TradeType;
import com.squad4.portfoliomangement.service.PositionService;
import com.squad4.portfoliomangement.util.CustomCode;

@ExtendWith(SpringExtension.class)
class PositionControllerTest {
	@Mock
	PositionService positionService;
	@InjectMocks
	PositionController positionController;
	@Test
	void testSuccess() {
		TradeType tradeType=TradeType.BUY;
		TradeEntryDto tradeEntryDto=new TradeEntryDto(1L, 1L, 10);
		ApiResponse apiResponse=ApiResponse.builder().message(CustomCode.CUSTOMER_SUCCESSFULLY_TRADED_MESSAGE).build();
		Mockito.when(positionService.tradeEntry(tradeType, tradeEntryDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=positionController.tradeEntry(tradeType, tradeEntryDto);
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	}
}
